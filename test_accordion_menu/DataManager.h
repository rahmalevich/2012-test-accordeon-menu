//
//  DataManager.h
//  test_accordion_menu
//
//  Created by Михаил Рахмалевич on 09.08.12.
//  Copyright (c) 2012 __My Company Name__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractNetworkRequest.h"

@interface DataManager : NSObject <AsyncRequestDelegate> {}

@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (DataManager *)sharedInstance;
- (void)loadPlacesData;
- (NSFetchedResultsController *)placesControllerWithDelegate:(id<NSFetchedResultsControllerDelegate>) delegate;

@end
