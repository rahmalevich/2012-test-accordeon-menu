//
//  main.m
//  test_accordion_menu
//
//  Created by Михаил Рахмалевич on 09.08.12.
//  Copyright (c) 2012 __My Company Name__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
