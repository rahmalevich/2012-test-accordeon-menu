//
//  GooglePlacesRequest.m
//
//  Created by Михаил Рахмалевич on 14.05.12.
//  Copyright (c) 2012 __My Company Name__. All rights reserved.
//

#import "GooglePlacesRequest.h"
#import "AbstractJSONResponse.h"

#define kGooglePlacesURL @"https://maps.googleapis.com/maps/api/place/search/json"
#define kGoogleClientKey @"AIzaSyDOlNjkbAD25mANNqG8L_BG9D4xSOVjByo"

@implementation GooglePlacesRequest
@synthesize type = _type;

- (GooglePlacesRequest *)init 
{
    self = [super init];
    if (self) {
        self.usesAsyncRequest = YES;
    }
    return self;
}

- (void)sendRequest
{
    NSDictionary *paramsDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"33.648210,-117.854215", @"location",
                                    @"1500", @"radius",
                                    _type, @"types",
                                    @"false", @"sensor",
                                    kGoogleClientKey, @"key",
                                    nil];
    self.requestURL = [AbstractNetworkRequest getGETUrlWithBase:kGooglePlacesURL andParams:paramsDict];
    [super sendRequest];
}

- (id) toObject {
    AbstractJSONResponse *response = [[AbstractJSONResponse alloc] initWithJSON:self.requestResponse];
    return [response autorelease];
}

@end
