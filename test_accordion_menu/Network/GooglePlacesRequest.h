//
//  GooglePlacesRequest.h
//
//  Created by Михаил Рахмалевич on 14.05.12.
//  Copyright (c) 2012 __My Company Name__. All rights reserved.
//

#import "AbstractNetworkRequest.h"

@interface GooglePlacesRequest : AbstractNetworkRequest

@property (nonatomic, copy) NSString *type;

@end
