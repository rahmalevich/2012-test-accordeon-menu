//
//  AbstractJSONModel.m
//  

#import "AbstractJSONResponse.h"
#import "SBJSON.h"

@interface AbstractJSONResponse (Private)
- (id)getValue:(NSString *)key fromDictionary:(NSDictionary *)dictionary;
@end

@implementation AbstractJSONResponse
@synthesize parsedDictionary;
@synthesize responseDictionary;
@synthesize parseError;
@synthesize apiError;
@synthesize errorDict;

- (id) initWithJSON: (NSString *) jsonString
{
	self = [super init];
	if (self) {
        parsedDictionary = nil;
        SBJSON *parser = [[SBJSON alloc] init];
        id parsedObject = [parser objectWithString:jsonString];
        
        if ([parsedObject isKindOfClass:[NSDictionary class]]) {
            self.parsedDictionary = parsedObject;
        }
        
        [parser release];
    }
    return self;
}

- (id)getParsedValue:(NSString *)key 
{
	return [self getValue:key fromDictionary:parsedDictionary];
}

- (id)getValue:(NSString *)key fromDictionary:(NSDictionary *)dictionary
{
    if (!dictionary)
        return nil;
    id value = [dictionary objectForKey:key];
    if ([value isKindOfClass:[NSNull class]]) 
        value = nil;
    return value;
}

- (void) dealloc {
	[parsedDictionary release];
    [responseDictionary release];
	[parseError release];
	[apiError release];   
	[super dealloc];
}

@end
