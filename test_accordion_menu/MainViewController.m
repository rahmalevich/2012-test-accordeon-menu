//
//  ViewController.m
//  test_accordion_menu
//
//  Created by Михаил Рахмалевич on 09.08.12.
//  Copyright (c) 2012 __My Company Name__. All rights reserved.
//

#import "MainViewController.h"
#import "DataManager.h"

@interface MainViewController () 

- (void)manageAccordionSection:(NSInteger)section;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@property (nonatomic, retain) NSFetchedResultsController *placesController;
@property (nonatomic, retain) NSMutableArray *expandedSections;

@end

@implementation MainViewController
@synthesize tableView = _tableView, placesController = _placesController, expandedSections = _expandedSections, activityIndicator = _activityIndicator;

#pragma mark - memory managment
- (void)dealloc
{
    [_placesController release];
    [_tableView release];
    [_expandedSections release];
    [_activityIndicator release];
    [super dealloc];
}

#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	
    if (!_placesController) {
        [_activityIndicator startAnimating];
        [[DataManager sharedInstance] loadPlacesData];
        self.placesController = [[DataManager sharedInstance] placesControllerWithDelegate:self];
        self.expandedSections = [NSMutableArray array];
    }
}

#pragma mark - table view datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[_placesController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    id <NSFetchedResultsSectionInfo> sectionInfo = [[_placesController sections] objectAtIndex:section];    
    if ([_expandedSections containsObject:[sectionInfo name]]) {
        numberOfRows = [sectionInfo numberOfObjects] + 1;
    } else {
        numberOfRows = 1;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseID = (indexPath.row == 0) ? @"expandableCell" : @"defaultCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseID] autorelease];
        if (indexPath.row == 0)
            cell.backgroundColor = [UIColor lightGrayColor];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        NSString *sectionTitle = [[[_placesController sections] objectAtIndex:indexPath.section] name];
        cell.textLabel.text = [sectionTitle stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    } else {        
        Place *corePlace = [_placesController objectAtIndexPath:[NSIndexPath indexPathForRow:(indexPath.row - 1) inSection:indexPath.section]];
        cell.textLabel.text = corePlace.name;
    }
}

#pragma mark - table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        [self manageAccordionSection:indexPath.section];
    }
}

- (void)manageAccordionSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[_placesController sections] objectAtIndex:section]; 
    NSInteger sectionCount = [sectionInfo numberOfObjects];
    NSMutableArray *sectionRowsIndexPaths = [NSMutableArray array];
    for (NSInteger i = 0; i < sectionCount; i++) {
        [sectionRowsIndexPaths addObject:[NSIndexPath indexPathForRow:(i+1) inSection:section]]; 
    }
         
    if ([_expandedSections containsObject:[sectionInfo name]]) {
        [_expandedSections removeObject:[sectionInfo name]];
        [_tableView deleteRowsAtIndexPaths:sectionRowsIndexPaths withRowAnimation:UITableViewRowAnimationFade];
    } else {
        [_expandedSections addObject:[sectionInfo name]];
        [_tableView insertRowsAtIndexPaths:sectionRowsIndexPaths withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - fetched results controller delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [_activityIndicator stopAnimating];
    [_tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type 
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [_tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;            
        case NSFetchedResultsChangeDelete:
            [_tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            [_expandedSections removeObject:[sectionInfo name]];
            break;            
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)aIndexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)aNewIndexPath 
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[controller sections] objectAtIndex:aNewIndexPath.section];
    
    if (![_expandedSections containsObject:[sectionInfo name]])
        return;
    
    UITableView *tableView = _tableView;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(aIndexPath.row + 1) inSection:aIndexPath.section];
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:(aNewIndexPath.row + 1) inSection:aNewIndexPath.section];
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [_tableView endUpdates];
}

@end
