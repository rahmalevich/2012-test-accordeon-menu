//
//  Place.h
//  test_accordion_menu
//
//  Created by Михаил Рахмалевич on 09.08.12.
//  Copyright (c) 2012 __My Company Name__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Place : NSManagedObject

@property (nonatomic, retain) NSString * placeId;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * rating;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * vicinity;

@end
